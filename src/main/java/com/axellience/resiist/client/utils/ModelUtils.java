package com.axellience.resiist.client.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Optional;

import org.apache.velocity.exception.ResourceNotFoundException;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.resource.Resource;
import org.genmymodel.gmmf.common.GenMyModelBinaryResourceImpl;
import org.opengroup.archimate.impl.ArchimatePackageImpl;

import com.genmymodel.archimatediag.impl.ArchimatediagPackageImpl;
import com.genmymodel.ecoreonline.graphic.GMMUtil;

public class ModelUtils
{

    private ModelUtils()
    {

    }

    public static Resource importArchimateModel(String projectId, byte[] data) throws IOException
    {
        ArchimatePackageImpl.init();
        ArchimatediagPackageImpl.init();

        GenMyModelBinaryResourceImpl resource =
                new GenMyModelBinaryResourceImpl(URI.createURI("genmymodel://"
                                                               + projectId
                                                               + "/archimate_imported"));
        InputStream inputStream = new ByteArrayInputStream(data);
        resource.load(inputStream, Collections.emptyMap());

        return resource;
    }

    public static String getFrequency(EModelElement modelElement)
    {
        EMap<String, String> details = getElementDetails(modelElement);
        if (!details.containsKey(ResiistConstants.FREQUENCY))
            throw new ResourceNotFoundException("ResiistConstants.FREQUENCY not found");

        return details.get(ResiistConstants.FREQUENCY);
    }

    public static String getObjectsToDetect(EModelElement modelElement)
    {
        EMap<String, String> details = getElementDetails(modelElement);
        if (!details.containsKey(ResiistConstants.OBJECTS_TO_DETECT))
            throw new ResourceNotFoundException("ResiistConstants.OBJECTS_TO_DETECT not found");

        return details.get(ResiistConstants.OBJECTS_TO_DETECT);
    }

    public static boolean hasFrequency(EModelElement modelElement)
    {
        if (hasAnnotation(modelElement)) {
            EMap<String, String> details = getElementDetails(modelElement);
            return details.containsKey(ResiistConstants.FREQUENCY);
        }
        return false;
    }

    private static EMap<String, String> getElementDetails(EModelElement modelElement)
    {
        Optional<EAnnotation> gmmAnnotation = GMMUtil.getGmmAnnotation(modelElement);
        if (!gmmAnnotation.isPresent())
            throw new ResourceNotFoundException("Element not found");

        return gmmAnnotation.get().getDetails();
    }

    public static boolean hasAnnotation(EModelElement modelElement)
    {
        return GMMUtil.getGmmAnnotation(modelElement).isPresent();
    }

    public static String getSimulationFilePath(EModelElement modelElement)
    {
        EMap<String, String> details = getElementDetails(modelElement);
        if (!details.containsKey(ResiistConstants.SIMULATION_FILE_PATH))
            throw new ResourceNotFoundException("Simulation file path not found");

        return details.get(ResiistConstants.SIMULATION_FILE_PATH);
    }

    public static String getElementName(EModelElement modelElement)
    {
        if (modelElement instanceof ENamedElement) {
            return ((ENamedElement) modelElement).getName();
        }
        return null;
    }
}
