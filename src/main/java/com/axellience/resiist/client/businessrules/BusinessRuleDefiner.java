package com.axellience.resiist.client.businessrules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;

import com.axellience.resiist.client.utils.ModelUtils;
import com.axellience.resiist.client.utils.ResiistConstants;
import com.genmymodel.api.dto.indicator.IndicatorDto;
import com.genmymodel.ecoreonline.graphic.GMMUtil;

public class BusinessRuleDefiner
{
    private static BusinessRuleDefiner instance = null;

    private Map<String, List<String>> criteriasAndIndicators      = new HashMap<>();
    private List<Map<String, String>> configurationParametersList = new ArrayList<>();

    private BusinessRuleDefiner()
    {
    }

    public static BusinessRuleDefiner getInstance()
    {
        if (instance == null)
            instance = new BusinessRuleDefiner();

        return instance;
    }

    public void interpretationConfiguration(ResourceImpl modelResource)
    {
        TreeIterator<EObject> iterator = modelResource.getAllContents();
        while (iterator.hasNext()) {
            EObject nextEObject = iterator.next();

            if (nextEObject instanceof EModelElement) {
                EModelElement modelElement = (EModelElement) nextEObject;
                if (ModelUtils.hasFrequency(modelElement)) {
                    Map<String, String> warnAndColors = new HashMap<>();

                    String elementId = GMMUtil.getUUID(modelElement);
                    EObject eObject = modelResource.getEObject(elementId);
                    Optional<EAnnotation> gmmAnnotation = GMMUtil.getGmmAnnotation(eObject);
                    EMap<String, String> details = gmmAnnotation.get().getDetails();

                    warnAndColors.put(ResiistConstants.ELEMENT_IDENTIFIER, elementId);
                    warnAndColors.put(ResiistConstants.FREQUENCY,
                                      details.get(ResiistConstants.FREQUENCY));

                    configurationParametersList.add(warnAndColors);
                }
            }
        }
    }

    public Integer getFrequency(String elementId)
    {
        return Integer.valueOf(configurationParametersList.stream()
                                                          .filter(conf -> conf.get(ResiistConstants.ELEMENT_IDENTIFIER)
                                                                              .equals(elementId))
                                                          .map(conf -> conf.get(ResiistConstants.FREQUENCY))
                                                          .findFirst()
                                                          .orElse(null));
    }

    public String getInterpretedValue(String indicatorName, String value)
    {
        return value;
    }

    public IndicatorDto toIndicatorDTO(String elementId, String indicatorName)
    {
        IndicatorDto indicatorDto = new IndicatorDto();
        indicatorDto.setElementId(elementId);
        indicatorDto.setKey(indicatorName);

        return indicatorDto;
    }

    public void setIndicatorForCriteria(String criteriaHeader, String indicatorHeader)
    {
        if (criteriasAndIndicators.get(criteriaHeader) == null) {
            criteriasAndIndicators.put(criteriaHeader, new ArrayList<String>());
        }
        criteriasAndIndicators.get(criteriaHeader).add(indicatorHeader);
    }

    public Map<String, List<String>> getCriteriasAndIndicators()
    {
        return criteriasAndIndicators;
    }
}