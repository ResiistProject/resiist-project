package com.axellience.resiist.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.axellience.resiist.client.businessrules.BusinessRuleDefiner;
import com.axellience.resiist.client.businessrules.EvaluationBusinessRuleDefiner;
import com.axellience.resiist.client.extraction.VideoSource;

@SpringBootApplication
public class ResiisDemonstratorVideo
{
    public static void main(String[] args)
    {
        /**
         * By default, Spring boot set a property - headless - to false,
         * blocking swing components (more precisely rising an headless
         * exception). One need to set it to 'true' to be able to use swing
         **/
        SpringApplicationBuilder builder =
                new SpringApplicationBuilder(ResiisDemonstratorVideo.class);
        builder.headless(false);
        builder.run(args);

        // ExtractionToIntegration extractionToIntegration =
        // new ExtractionToIntegration(Interpreter.getInstance());
        // NOTICE - Define and use your specific interpreter for aggregated
        // indicator
        ExtractionToIntegration extractionToIntegration =
                new ExtractionToIntegration(BusinessRuleDefiner.getInstance(),
                                            EvaluationBusinessRuleDefiner.getInstance());

        extractionToIntegration.connexionToModelingPlatform();

        extractionToIntegration.getModelFromServerAsBinary();

        extractionToIntegration.integrationConfiguration();
        extractionToIntegration.interpretationConfiguration();

        extractionToIntegration.connexionToDataSourceAsVideo();

        extractionToIntegration.start();

        new VideoSource().start();
    }
}