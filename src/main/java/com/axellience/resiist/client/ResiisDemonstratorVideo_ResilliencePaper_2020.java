package com.axellience.resiist.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.axellience.resiist.client.businessrules.BusinessRuleDefiner;
import com.axellience.resiist.client.businessrules.EvaluationBusinessRuleDefiner;
import com.axellience.resiist.client.extraction.VideoSource;

@SpringBootApplication
public class ResiisDemonstratorVideo_ResilliencePaper_2020
{
    public static void main(String[] args)
    {
        /**
         * By default, Spring boot set a property - headless - to false,
         * blocking swing components (more precisely rising an headless
         * exception). One need to set it to 'true' to be able to use swing
         **/
        SpringApplicationBuilder builder =
                new SpringApplicationBuilder(ResiisDemonstratorVideo_ResilliencePaper_2020.class);
        builder.headless(false);
        builder.run(args);

        /** Video and simulation data source **/
        ExtractionToIntegration extractionToIntegrationAsVideo =
                new ExtractionToIntegration(BusinessRuleDefiner.getInstance(),
                                            EvaluationBusinessRuleDefiner.getInstance());

        extractionToIntegrationAsVideo.connexionToModelingPlatform();

        extractionToIntegrationAsVideo.getModelFromServerAsBinary();

        extractionToIntegrationAsVideo.integrationConfiguration();
        extractionToIntegrationAsVideo.interpretationConfiguration();

        extractionToIntegrationAsVideo.connexionToDataSourcesAsRealTypeSimulation();

        extractionToIntegrationAsVideo.connexionToDataSourceAsVideo();

        extractionToIntegrationAsVideo.start();

        new VideoSource().start();
    }
}