package com.axellience.resiist.client;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.opengroup.archimate.impl.ModelImpl;

import com.axellience.resiist.client.businessrules.BusinessRuleDefiner;
import com.axellience.resiist.client.businessrules.EvaluationBusinessRuleDefiner;
import com.axellience.resiist.client.extraction.DataSource;
import com.axellience.resiist.client.integration.IntegrationPlatform;
import com.axellience.resiist.client.utils.CsvUtil;
import com.axellience.resiist.client.utils.ResiistConstants;
import com.axellience.resiist.client.utils.ihm.AccessApplication;

public class ExtractionToIntegration extends Thread
{
    private IntegrationPlatform           integrationPlatform = IntegrationPlatform.getInstance();
    private DataSource                    dataSource          = new DataSource();
    private BusinessRuleDefiner           businessRuleDefiner;
    private EvaluationBusinessRuleDefiner evaluationBusinessRuleDefiner;
    private boolean                       simulation          = false;
    private boolean                       random;
    private String                        projectId;

    public ExtractionToIntegration(BusinessRuleDefiner businessRuleDefiner,
                                   EvaluationBusinessRuleDefiner evaluationBusinessRuleDefiner)
    {
        this.businessRuleDefiner = businessRuleDefiner;
        this.evaluationBusinessRuleDefiner = evaluationBusinessRuleDefiner;
    }

    public void connexionToModelingPlatform()
    {
        AccessApplication accessApplication = AccessApplication.getInstance(integrationPlatform);
        while (true) {
            boolean loginDone = accessApplication.isLoginDone();
            try {
                Thread.sleep(2);
                if (loginDone) {
                    projectId = accessApplication.getProjectId();
                    break;
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void connexionToDataSourceAsVideo()
    {
        dataSource.initConnexionWithVideoCaptor();
    }

    public void connexionToDataSourcesAsRealTypeSimulation()
    {
        String evolutionFunction = integrationPlatform.getEvolutionFunction();
        List<Map<String, String>> allSimulationFilePath =
                integrationPlatform.getAllElementAndObjectToDetectAndFrequenciesAndSimulationFilePaths();
        for (Map<String, String> elementIDAndSimulationFilePath : allSimulationFilePath) {

            String simulationFilePath =
                    elementIDAndSimulationFilePath.get(ResiistConstants.SIMULATION_FILE_PATH);
            if (evolutionFunction == null) {
                dataSource.initConnexionForSimulationWithRealTypeValue(simulationFilePath);
            } else if (evolutionFunction.equals("random")) {
                dataSource.initConnexionForSimulationWithRandomValue(simulationFilePath);
                dataSource.setLessZeroRandomValues(false);
                random = true;
            }
        }
        simulation = true;
    }

    public void getModelFromServerAsBinary()
    {
        integrationPlatform.getArchimateModel(projectId);
    }

    public void integrationConfiguration()
    {
        integrationPlatform.integrationConfiguration(businessRuleDefiner);
    }

    public void interpretationConfiguration()
    {
        integrationPlatform.interpretationConfiguration();
    }

    @Override
    public void run()
    {
        List<Map<String, Object>> allElementWidgetIDAndDTOs =
                integrationPlatform.allElementWidgetIDAndDTOs.stream()
                                                             .filter(element -> !((String) element.get(ResiistConstants.SIMULATION_FILE_PATH)).isEmpty()
                                                                                || element instanceof ModelImpl)
                                                             .collect(Collectors.toList());

        while (true) {
            if (dataSource.canSendValue()) {
                dataSource.requestCriteriaAndValue();
                String elementId = dataSource.getElementIdFromCaptor();
                String value = dataSource.getValueFromCaptor();
                Float resilience =
                        evaluationBusinessRuleDefiner.evaluateElementResilience(elementId, value);
                Float modelResilience =
                        evaluationBusinessRuleDefiner.evaluateModelResilience(elementId,
                                                                              resilience);
                String resilienceAlertColor =
                        evaluationBusinessRuleDefiner.getResilienceAlertColor(elementId,
                                                                              resilience);

                sendValue(elementId, value, resilience, resilienceAlertColor, modelResilience);
            }

            if (dataSource.canSendSimulatedValue()) {
                for (Map<String, Object> elementWidgetIDAndDTO : allElementWidgetIDAndDTOs) {
                    String elementId =
                            (String) elementWidgetIDAndDTO.get(ResiistConstants.ELEMENT_IDENTIFIER);
                    String filePath =
                            (String) elementWidgetIDAndDTO.get(ResiistConstants.SIMULATION_FILE_PATH);
                    if (filePath != null && new CsvUtil().isACSVFile(filePath)) {
                        Integer frequency = businessRuleDefiner.getFrequency(elementId);
                        if (dataSource.isSendingTime(frequency, elementId)) {
                            String simulatedValue =
                                    dataSource.getSimulatedValue(allElementWidgetIDAndDTOs.indexOf(elementWidgetIDAndDTO),
                                                                 elementId);
                            Float resilience =
                                    evaluationBusinessRuleDefiner.evaluateElementResilience(elementId,
                                                                                            simulatedValue);
                            Float modelResilience =
                                    evaluationBusinessRuleDefiner.evaluateModelResilience(elementId,
                                                                                          resilience);
                            String resilienceAlertColor =
                                    evaluationBusinessRuleDefiner.getResilienceAlertColor(elementId,
                                                                                          resilience);
                            sendValue(elementId,
                                      simulatedValue,
                                      resilience,
                                      resilienceAlertColor,
                                      modelResilience);
                        }
                    }
                }
            }

            // if (dataSource.canSendRandomSimulatedValue() &&
            // dataSource.isSendingTime()) {
            // String value =
            //
            // String.valueOf(dataSource.getRandomSimulatedValue(interpreter.getMinLimitValueForIndicator(indicatorFromSimulationConf),
            // //
            // interpreter.getMaxLimitValueForIndicator(indicatorFromSimulationConf)));
            // sendValue(indicatorFromSimulationConf, value);
            // }
            // if (dataSource.isSendingTime()) {
            // integrationPlatform.estimateGlobalResilience();
            // }
        }

    }

    // @Override
    // public void run()
    // {
    //// if (!simulation) {
    // while (dataSource.canSendValue()) {
    // dataSource.requestCriteriaAndValue();
    // sendValue(dataSource.getIndicatorFromCaptor(),
    // dataSource.getValueFromCaptor());
    // }
    //// } else
    // runSimulation();
    // }
    //
    // private void runSimulation()
    // {
    // String indicatorFromSimulationConf =
    // dataSource.getIndicatorFromSimulationConf();
    // if (!random) {
    // while (dataSource.canSendSimulatedValue()) {
    // if (dataSource.isSendingTime()) {
    // sendValue(indicatorFromSimulationConf, dataSource.getSimulatedValue());
    // }
    // }
    // } else {
    // if (dataSource.isFromOneIntialValue()) {
    // String value = dataSource.getInitialValue();
    // dataSource.setPreviousRandomValue(Integer.valueOf(value));
    // sendValue(indicatorFromSimulationConf, value);
    // }
    // while (dataSource.canSendRandomSimulatedValue()) {
    // if (dataSource.isSendingTime()) {
    // String value =
    // String.valueOf(dataSource.getRandomSimulatedValue(interpreter.getMinLimitValueForIndicator(indicatorFromSimulationConf),
    // interpreter.getMaxLimitValueForIndicator(indicatorFromSimulationConf)));
    // sendValue(indicatorFromSimulationConf, value);
    // }
    // }
    // }
    // }

    private void sendValue(String indicatorId, String value, Float resilience,
                           String resilienceAlertColor, Float modelResilience)
    {
        integrationPlatform.setFnForIndicator(indicatorId,
                                              businessRuleDefiner.getInterpretedValue(indicatorId,
                                                                                      value));
        integrationPlatform.setColorForIndicator(indicatorId, resilienceAlertColor);
        integrationPlatform.setResilienceForIndicator(indicatorId, resilience);
        integrationPlatform.setModelResilience(modelResilience);
    }
}